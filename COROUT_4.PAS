{ ����� ��� 4-�� ������୮� }
Unit Corout_4;

Interface

uses OBJECTS, DOS;

Type
    arptr    = ^artype;
    artype   = array[0..999] of word; {��� �⥪ - 1000 ᫮�}

    PDescriptor  = ^procdesc;
    procdesc = record
                     ssreg,
                     spreg  : word;
                     StackAdr: arptr;
                     list: pCollection;
                     tact: longint;

               end; {record}

procedure StopProcess(proc: PDescriptor);
procedure RestartProcess(proc: PDescriptor);
procedure DelayP(Delay: longint);

procedure StopDisp;
procedure CreateProcess(body: pointer; Var proc: PDescriptor);
procedure KillProcess(proc: PDescriptor);
procedure SelfKill;
procedure StartDisp;

{--------------------------------------------------------------------------}
Implementation

Var
   T: longint;
   time: pointer;
   main, proc1, proc2, proc3: PDescriptor;
   ReadyList:TCollection;
   StopList:TCollection;
   StopTList:TCollection;
   KillList:TCollection;
   Curproc: PDescriptor;
{--------------------------------------------------------------------------}

procedure EnableInterrupt;
  Assembler;
  Asm
    sti
  end;
{--------------------------------------------------------------------------}

procedure BanInterrupt;
 Assembler;
  Asm
    Cli
  end; {Asm}
{--------------------------------------------------------------------------}

procedure StopProcess(proc:PDescriptor);
begin
  BanInterrupt;
  ReadyList.Delete(proc);
  proc^.list:=nil;
  StopList.Insert(proc);
  proc^.list:=@StopList;
  EnableInterrupt;
end; {StopProcess}
{--------------------------------------------------------------------------}

procedure RestartProcess(proc:PDescriptor);
var List: pCollection;
begin
  BanInterrupt;
  List:= proc^.List;
  list^.Delete(proc);
  proc^.list:= nil;
  ReadyList.Insert(proc);
  proc^.list:= @ReadyList;
  EnableInterrupt;
end; {RestartProcess}
{--------------------------------------------------------------------------}
procedure Activisation;
Var cr: PDescriptor;
    i, j: integer;
begin
  BanInterrupt;
  if (StopTList.count <> 0) then
  begin
    i := 0;
    while (i <= (StopTList.count -1) ) do
      begin
      cr := StopTList.at(i);
        {Writeln('Tact= ',cr^.Tact);}
        if cr^.Tact = T then
          begin
            StopTList.Delete(cr);
            cr^.list:= nil;
            ReadyList.Insert(cr);
            cr^.list:= @ReadyList;
         end
        else
          Inc(i);
      end;
  end;
  EnableInterrupt;
end; {Activisation}
{--------------------------------------------------------------------------}

procedure NewProcess(body : pointer; Var proc : PDescriptor);
Var
     ar : arptr;
begin
     New(proc);
     New(ar);

     with proc^ do
     begin
          ssreg := seg(ar^);
          spreg := ofs(ar^) + 1998 - 14;
          StackAdr := ar;
          memw[ssreg:spreg+2] := ofs(body^);
          memw[ssreg:spreg+4] := seg(body^);
     end {with};
end {NewProcess};
{--------------------------------------------------------------------------}

procedure CreateProcess(body:pointer; Var proc:PDescriptor);
begin
  BanInterrupt;
  NewProcess(body, proc);
  ReadyList.Insert(proc);
  proc^.List:= @ReadyList;
  EnableInterrupt;
end; {CreateProcess}
{-------------------------------------------------------}

procedure KillProcess(proc:PDescriptor);
Var
  List:pCollection;
begin
  BanInterrupt;
  List:= proc^.List;
  if List <> nil then
    List^.Delete(proc);
  proc^.list:= nil;
  KillList.Insert(proc);
  EnableInterrupt;
end; {KillProcess}
{-------------------------------------------------------}
{--------------------------------------------------------------------------}
procedure ClearKillList;
var KillProc: PDescriptor;
begin
  while KillList.Count <> 0 do
    begin
      KillProc:= KillList.At(0);
      KillList.AtDelete(0);
      Dispose(KillProc^.StackAdr);
      Dispose(KillProc);
    end;
end; {ClearKillList}
{--------------------------------------------------------------------------}

{ ��।�� �ࠢ����� �� ����� � ����� }
{ �室�� ��ࠬ���� - 2 ���ਯ��: }
{   OldProc - ���ਯ�� ����� �� ���ண� ��४��砥��� } 
{   NewProc - ���ਯ�� ����� � ����� ��४��砥��� }
{$F+}
procedure Transfer( OldProc, NewProc : PDescriptor ); assembler;
asm                    {��������� ��᫥ Call Transfer
                        ����⠢��� push bp; mov bp,sp}
  { ����� �� �����-� ��楤��� �� ��뢠�� Transfer, � �⥪ �����뢠���� ���� ᫥���饩 ������� ��᫥ �맮�� Transfer }
  les di,OldProc
  mov es:[di],ss      { OldProc.SS := ss; }
  mov es:[di+2],sp    { OldProc.SP := sp; ���� ������ � sp+2}

  { ������塞 ���� ������ �� ���� �� NewProc }
  les di,NewProc
  mov ss,es:[di]      { ss := newproc.SS; }
  mov sp,es:[di+2]    { sp := newproc.SP; }

  pop bp      {��⠫������� bp �뢮��� �⥪ �� ���� ������}
  sti
  ret 8
       {��⮫��㫨 8 ���⮢ - 4 ᫮�� - ���祭�� oldproc � newproc}
end {Transfer};
{$F-}

{$F+}
procedure Handler; interrupt;
  var
    OldProc:PDescriptor;
  begin
    Inc(T);
    BanInterrupt;
    ClearKillList;
    Activisation;
    Port[$20]:= $20;
    OldProc:= CurProc;
    ReadyList.Insert(CurProc);
    CurProc^.list:= @ReadyList;
    CurProc:= ReadyList.at(0);
    ReadyList.atDelete(0);
    CurProc^.list:= nil;
    Transfer(OldProc, CurProc);
  end; {Handler}
{$F-}

{-----------------------------------------------------}
procedure SelfKill;
var KillProc: PDescriptor;
begin
  BanInterrupt;
  KillProc:= CurProc;
  CurProc:= ReadyList.At(0);
  ReadyList.atDelete(0);
  CurProc^.list:= nil;
  KillProcess(KillProc);
  Transfer(KillProc, CurProc);
end; {SelfKill}
{-----------------------------------------------------}
procedure DelayP(Delay: longint);
Var OldProc: PDescriptor;
begin
  BanInterrupt;
  OldProc:= CurProc;
  OldProc^.Tact:= T + Delay;
  StopTList.Insert(OldProc);
  OldProc^.list:= @StopTList;
  CurProc:= ReadyList.at(0);
  CurProc^.list:= nil;
  ReadyList.atDelete(0);
  Transfer(OldProc, CurProc);
end; {DelayP}
{--------------------------------------------------------------------------}


procedure StartDisp;
begin
  BanInterrupt;
  GetIntVec($1C,time);
  SetIntVec($1C,addr(Handler));
  T:= 0;
  CurProc:=ReadyList.at(0);
  ReadyList.atDelete(0);
  CurProc^.list:= nil;
  Transfer(main, CurProc);
end; {StartDisp}
{-------------------------------------------------------}

procedure StopDisp;
var KillProc: PDescriptor;
begin
  BanInterrupt;
  while (StopTList.count <> 0) do
  begin
    ReadyList.Insert(StopTList.At(0));
    StopTList.AtDelete(0);
  end;
  ReadyList.Insert(CurProc);
  while ReadyList.Count <> 0 do
    begin
      KillProc:= ReadyList.At(0);
      ReadyList.AtDelete(0);
      Dispose(KillProc^.StackAdr);
      Dispose(KillProc);
    end;
  SetIntVec($1C, time);
  Transfer(CurProc, main);
end; {StopDisp}
{-------------------------------------------------------}

begin
  New(main);
  ReadyList.Init(10,5);
  StopList.Init(10,5);
  StopTList.Init(10,5);
  KillList.Init(10,5);
end.






